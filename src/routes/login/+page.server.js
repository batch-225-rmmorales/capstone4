import { prisma, dbConnect, dbDisconnect } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		let email;
		let password;
		await event.request.formData().then((data) => {
			email = data.get('email');
			password = data.get('password');
		});

		const user = await prisma.user.findUnique({
			where: {
				email: email
			}
		});

		if (user.password == password) {
			event.cookies.set('jwt', 'mors token yo', {
				// send cookie for every page
				path: '/',
				// server side only cookie so you can't use `document.cookie`
				httpOnly: true,
				// only requests from same site can send cookies
				// https://developer.mozilla.org/en-US/docs/Glossary/CSRF
				sameSite: 'strict',
				// only sent over HTTPS in production
				// secure: process.env.NODE_ENV === 'production',
				// set cookie to expire after a month
				maxAge: 60 * 60 * 3
			});
			throw redirect(302, '/');
		}

		throw redirect(302, '/login');
	}
};
