import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const GET = async (event) => {
	const carts = await prisma.cart.findMany();
	return json(carts);

	// messages = JSON.parse(JSON.stringify(messages));
	// messages = messages.reverse();
};

// let data = {
// 	carts: [
// 		{
// 			id: 1,
// 			seller: 'Mors',
// 			buyer: 'Sam',
// 			quantity: 3,
// 			total: 5200,

// 			products: [
// 				{
// 					id: 59,
// 					title: 'Tshirt',
// 					image: 'https://ph-test-11.slatic.net/p/5d81a4981dca265f0ea4b53a3ce7a27e.jpg',
// 					quantity: 2,
// 					price: 100,
// 					total: 200
// 				},
// 				{
// 					id: 68,
// 					title: 'Watch',
// 					image: 'https://m.media-amazon.com/images/I/71VjM5LOeYL._AC_UL1500_.jpg',
// 					quantity: 1,
// 					price: 5000,
// 					total: 5000
// 				}
// 			]
// 		},
// 		{
// 			id: 2,
// 			buyer: 'Mors',
// 			seller: 'Sam',
// 			quantity: 4,
// 			total: 1900,

// 			products: [
// 				{
// 					id: 59,
// 					title: 'Basketball',
// 					image:
// 						'https://artwork.espncdn.com/categories/cd70a58e-a830-330c-93ed-52360b51b632/1x1Feature/1440_201903062023.jpg',
// 					quantity: 3,
// 					price: 500,
// 					total: 1500
// 				},
// 				{
// 					id: 68,
// 					title: 'Toy',
// 					image: 'https://m.media-amazon.com/images/I/81c4L1ccJzL._AC_SL1500_.jpg',
// 					quantity: 1,
// 					price: 400,
// 					total: 400
// 				}
// 			]
// 		}
// 	]
// };
