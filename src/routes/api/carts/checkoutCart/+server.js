import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const POST = async (event) => {
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	// find if seller/buyer pair exist
	console.log(body);

	const findCart = await prisma.cart.findUnique({
		where: {
			id: body.id
		}
	});
	// console.log('findCart');
	// console.log(findCart);
	let productsToCheck = findCart.products;
	//check if enough inventory
	let enoughInventory = true;
	let findProduct;
	let newStock;
	await productsToCheck.forEach(async (item) => {
		findProduct = await prisma.product.findUnique({
			where: {
				id: item.id
			}
		});
		console.log('item');
		console.log(item);
		console.log('findProduct');
		console.log(findProduct);
		console.log('boolean');
		console.log(Number(findProduct.stock) < Number(item.quantity));
		if (Number(findProduct.stock) < Number(item.quantity)) {
			enoughInventory = false;
			console.log('enoughInventory1');
			console.log(enoughInventory);
		}
	});

	console.log('enoughInventory2');
	console.log(enoughInventory);
	if (enoughInventory) {
		productsToCheck.forEach(async (product) => {
			findProduct = await prisma.product.findUnique({
				where: {
					id: product.id
				}
			});
			newStock = findProduct.stock - product.quantity;
			findProduct = await prisma.product.update({
				where: {
					id: product.id
				},
				data: {
					stock: newStock
				}
			});
		});
		const updateCart = await prisma.cart.update({
			where: {
				id: body.id
			},
			data: {
				isPaid: true
			}
		});

		return json({ message: 'cart is fully paid', data: updateCart });
	} else {
		return json({ message: 'not enough stock to perform request' });
	}
};

// messages = JSON.parse(JSON.stringify(messages));
// messages = messages.reverse();
