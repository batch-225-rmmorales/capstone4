import { writable, derived } from 'svelte/store';
export const persons = writable([]);
export const messages = writable([]);
