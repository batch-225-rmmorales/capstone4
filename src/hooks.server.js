// https://next-auth.js.org/adapters/prisma

import { SvelteKitAuth } from '@auth/sveltekit';
import GitHub from '@auth/core/providers/github';
import Facebook from '@auth/core/providers/facebook';
import Google from '@auth/core/providers/google';
import NextAuth from 'next-auth';
// import CredentialsProvider from 'next-auth/providers/credentials';
import prisma from '$lib/prisma';
import { PrismaAdapter } from '@next-auth/prisma-adapter';
import {
	GITHUB_ID,
	GITHUB_SECRET,
	FACEBOOK_ID,
	FACEBOOK_SECRET,
	GOOGLE_ID,
	GOOGLE_SECRET
} from '$env/static/private';

export const handle = SvelteKitAuth({
	adapter: PrismaAdapter(prisma),
	// session: { strategy: 'jwt' },
	providers: [
		// credentials is only for JWT... not persisted in database
		// CredentialsProvider({
		// 	// The name to display on the sign in form (e.g. "Sign in with...")
		// 	name: 'Email & Password',
		// 	// `credentials` is used to generate a form on the sign in page.
		// 	// You can specify which fields should be submitted, by adding keys to the `credentials` object.
		// 	// e.g. domain, username, password, 2FA token, etc.
		// 	// You can pass any HTML attribute to the <input> tag through the object.
		// 	credentials: {
		// 		email: { label: 'Email', type: 'text' },
		// 		password: { label: 'Password', type: 'password' }
		// 	},
		// 	async authorize(credentials, req) {
		// 		// Add logic here to look up the user from the credentials supplied
		// 		// const user = { id: '1', name: 'J Smith', email: 'jsmith@example.com' };
		// 		// mors: dito dapat may verification rin ng password sa find
		// 		const user = await prisma.user.findUnique({
		// 			where: {
		// 				email: credentials.email
		// 			}
		// 		});

		// 		if (user) {
		// 			console.log(user);
		// 			// Any object returned will be saved in `user` property of the JWT
		// 			return user;
		// 		} else {
		// 			// If you return null then an error will be displayed advising the user to check their details.
		// 			return null;

		// 			// You can also Reject this callback with an Error thus the user will be sent to the error page with the error message as a query parameter
		// 		}
		// 	},

		// }),

		Google({
			// profile(profile) {
			// 	return { role: profile.role ?? 'user', clientId: GOOGLE_ID, clientSecret: GOOGLE_SECRET };
			// }
			clientId: GOOGLE_ID,
			clientSecret: GOOGLE_SECRET
		}),
		GitHub({ clientId: GITHUB_ID, clientSecret: GITHUB_SECRET }),
		Facebook({
			// profile(profile) {
			// 	return { role: profile.role ?? 'user', clientId: GOOGLE_ID, clientSecret: GOOGLE_SECRET };
			// }
			clientId: FACEBOOK_ID,
			clientSecret: FACEBOOK_SECRET
		})
	],
	callbacks: {
		async session({ session, token, user }) {
			// Send properties to the client, like an access_token and user id from a provider.
			session.user.username = user.username;

			return session;
		}
	}
	// callbacks: {
	// 	session({ session, user }) {
	// 		session.user.role = user.role;
	// 		return session;
	// 	}
	// }
});

// https://next-auth.js.org/configuration/providers/credentials
